﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2DArraySort
{
	//This is a class
	//This is a class - Jill

        //This is a class - Joe2

	//This is a class - Jill2

    class Program
    {
        static void Main(string[] args)
        {

            /*int[,] mArray = { {3,4,5},
                              {8,2,1},
                              {1,2,1}};*/


            char c = 'y';

            int[,] mArray = Utilities.GetValues();
            Console.WriteLine("Array before sort");
            Utilities.Display(mArray);
            Utilities.Sort(mArray, true);
            Console.WriteLine("Array after sort");
            Utilities.Display(mArray);
            Console.WriteLine("\n\n");

            ConsoleKeyInfo k = new ConsoleKeyInfo();
            bool flag = true;
            while (flag == true || k.KeyChar == 'y' || k.KeyChar == 'Y')
            {
                Console.WriteLine("\nEnter the number to serach");
                int n = Convert.ToInt32(Console.ReadLine());

                if (Utilities.BinarySearch(mArray, n)) Console.WriteLine("Number {0} found", n);
                else Console.WriteLine("Number {0} NOT found", n);
                flag = false;
                Console.WriteLine("\nDo you want to continue, press Y/y or n/N");
                k = Console.ReadKey();
            }

            Console.ReadLine();
        }


		
	}
}
