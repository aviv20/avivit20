﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2DArraySort
{
    class Utilities
    {

		public static int[,] GetValues()
		{
			Console.WriteLine("Enter the number of rows");
			int rows = Convert.ToInt32(Console.ReadLine());

			Console.WriteLine("Enter the number of columns");
			int cols = Convert.ToInt32(Console.ReadLine());

			int[,] nArray = new int[rows, cols];

			for(int i=0;i< rows;i++)
			{
				for (int j = 0; j < cols; j++)
				{
					Console.WriteLine("Enter the [{0},{1}]th data ",i, j);
					nArray[i, j] = Convert.ToInt32(Console.ReadLine());
				}
			}
			return nArray;

		}




		public static void Sort(int[,] obArray, bool b=true)
		{
			int mX = 0, mY = 0;

			try
			{
				if (b)
				{
					for (int i = 0; i <= obArray.GetUpperBound(0); i++)
					{
						for (int j = 0; j <= obArray.GetUpperBound(1); j++)
						{
							int min = FindMin(obArray, i, j, ref mX, ref mY);
							if (min < (obArray[i, j]))
							{
								int tmp = obArray[i, j];
								obArray[i, j] = min;
								obArray[mX, mY] = tmp;
							}
						}
					}
				}
				else
				{
					int[] nArray1D = ConvertTo1D(obArray);
					Array.Sort(nArray1D);
					ConvertTo2D(obArray, nArray1D);

				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Exception happened - Sort " + ex.Message);
			}
		}

		public static int FindMin(int[,] obArray, int i, int j, ref int minX, ref int minY)
		{

			int min = obArray[i, j];
			try
			{
				for (int p = i; p <= obArray.GetUpperBound(0); p++)
				{
					for (int q = j; q <= obArray.GetUpperBound(1); q++)
					{
						if (obArray[p, q] < min)
						{
							min = obArray[p, q];
							minX = p;
							minY = q;
						}
					}

					j = 0;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Exception happened - FindMin " + ex.Message);
			}

			return min;
		}

		static int[] ConvertTo1D(int[,] st)
		{
			int[] st1 = new int[st.Length];
			int c = 0;
			for (int i = 0; i <= st.GetUpperBound(0); i++)
			{
				for (int j = 0; j <= st.GetUpperBound(1); j++)
				{
					st1[c++] = st[i, j];

				}

			}
			return st1;
		}

		static void ConvertTo2D(int[,] st, int[] st1)
		{
			int c = 0;
			for (int i = 0; i <= st.GetUpperBound(0); i++)
			{
				for (int j = 0; j <= st.GetUpperBound(1); j++)
				{
					st[i, j] = st1[c++];

				}

			}

		}

		public static void Display(int[,] obArray)
		{
			for(int i=0;i<obArray.GetLength(0);i++)
			{
				for (int j = 0; j < obArray.GetLength(1);j++)
				{
					Console.Write(obArray[i,j] + "  ");
				}
				Console.WriteLine("");
		       
			}
		}


		public static bool BinarySearch(int[,] obArray, int key)
		{

			try
			{
				int startIndex = 0;
				int rows = obArray.GetLength(0);
				int cols = obArray.GetLength(1);
				int endIndex = rows * cols - 1;

				while(startIndex<= endIndex)
				{
					int mid = startIndex + (endIndex - startIndex) / 2;
					int rowIndex = mid / cols;
					int colIndex = mid % cols;
					if (key == obArray[rowIndex, colIndex]) return true;
					else if (key < obArray[rowIndex, colIndex]) endIndex = mid - 1;
					else startIndex = mid + 1;
				}




			}
			catch(Exception ex)
			{
				Console.WriteLine("Exception happened - BinarySearch " + ex.Message);
			}

			return false;


		}






	}
}
